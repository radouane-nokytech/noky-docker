# Installing Magento with Docker

This guide provides steps to install Magento using Docker.

## Prerequisites
- Docker and Docker Compose installed on your system.
- Magento project ready for Docker integration.

## Steps

### 1. Add Docker Folder to Magento Project

First, add the Docker configuration folder to your Magento project. This folder should contain all necessary Docker configuration files.

### 2. Modify Docker Images and Database Access in .env File

Edit the `.env` file to specify the correct Docker image versions and database access credentials. 

**Note:** Ensure the Docker images with the defined tags exist on the Docker Hub website.

### 3. Launch Docker Containers

Use the following command to start the Docker containers:

```bash
docker compose up -d
```

### 4. Check Running Containers
To see the running containers and their IDs, use:

```bash
docker ps
```

### 5. Access the Database for Import
To import a SQL file into the database, use:

```bash
docker exec -i [mysql_container_name] mysql -u[username] -p[password] [DB name]
```

### 6. Access the Web Container
To access the web container, execute:
```bash
docker exec -it [Container ID] bash
```



## Acces
    - phpmyadmin : localhost:8081
    - website : localhost:8080