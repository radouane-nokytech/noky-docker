# FROM php:8.2.4-apache
FROM ${PHP_APACHE_IMAGE}

# Install various PHP extensions
RUN apt-get update \
    && apt-get install -y \
        libxml2-dev \
        libbz2-dev \
        libjpeg-dev \
        libpng-dev \
        libfreetype6-dev \
        libssl-dev \
        libxslt1-dev \
        libzip-dev \
        libc-client-dev \
        libkrb5-dev \
    && docker-php-ext-install soap bcmath gd xsl zip sockets \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-install -j$(nproc) gd imap \
    && docker-php-ext-install pdo_mysql intl

# Enable Apache rewrite module
RUN a2enmod rewrite

COPY ./php/php.ini /usr/local/etc/php/